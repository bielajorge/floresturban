import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { InicioService } from 'app/inicio/inicio.service';

@Component({
  selector: 'mt-end',
  templateUrl: './end.component.html'
})
export class EndComponent implements OnInit {

  images = new Array();
  constructor(private inicioService: InicioService) { }

  ngOnInit() {
    this.images = [];

    this.escolheImagens();
  }

  escolheImagens() {
    console.log(this.inicioService.plantas)
    this.inicioService.plantas.forEach(planta => {

      if (planta.nome === "IPÊ-BRANCO")
        this.images.push('assets/img/ipe_branco.png');

      else if (planta.nome === "IPÊ-AMARELO")
        this.images.push('assets/img/ipe_amarelo.png');

      else if (planta.nome === "GOIABEIRA")
        this.images.push('assets/img/goiabeira.png');

      else if (planta.nome === "COPAÍBA")
        this.images.push('assets/img/copaiba.png');

      else if (planta.nome === "PAU-BRASIL")
        this.images.push('assets/img/pau_brasil.png');

      else if (planta.nome === "ANGICO FERRO")
        this.images.push('assets/img/angico_ferro.png');

      else if (planta.nome === "CAROBÁ")
        this.images.push('assets/img/caroba.png');

      else if (planta.nome === "MONGUBA")
        this.images.push('assets/img/monguba.png');

      else if (planta.nome === "PITOMBEIRA")
        this.images.push('assets/img/pitombeira.png');

      console.log(planta)
    });
  }
}



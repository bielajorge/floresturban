
class Participante {
    constructor(
        public id: number,
        public nome: String,
        public email: String,
        public acessos: number,
        public perfil: Perfil[] = []
    ) { }
}

class Perfil {
    constructor(
        public espaco: number,
        public gostaDeArvoreFrutifera: number,
        public jaPlantouArvore: number) { }
}

export { Participante, Perfil }


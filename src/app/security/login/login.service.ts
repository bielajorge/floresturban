import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs';

import { Participante } from './participantes.model';
import { FLORESTA_API } from 'app/app.api';
import { InicioService } from 'app/inicio/inicio.service';

@Injectable()
export class LoginService {

    user: Participante
    lastUrl: string

    constructor(
        private http: HttpClient,
        private router: Router) { }

    isLoggedIn(): boolean {
        return this.user !== undefined
    }

    login(email: string, nome: string): Observable<any> {
        return this.http.post<Participante>(`${FLORESTA_API}/participante/init-quiz`, { "nome": `${nome}`, "email": `${email}` })
    }

    logout() {
        this.user = undefined
        this.router.navigate(['/login'])
    }

    handleLogin(path: string = this.lastUrl) {
        this.router.navigate(['/login'])
    }
}

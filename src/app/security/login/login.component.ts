import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router'
import { LoginService } from './login.service';
import { NotificationService } from '../../shared/messages/notification.service';
import { Participante } from './participantes.model';
import { InicioService } from 'app/inicio/inicio.service';


@Component({
  selector: 'mt-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup
  navigateTo: string

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private notificationService: NotificationService,
    private activateRoute: ActivatedRoute,
    private router: Router,
    private inicioService: InicioService) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: this.fb.control('', [Validators.required, Validators.email]),
      nome: this.fb.control('', [Validators.required])
    })

    this.navigateTo = this.activateRoute.snapshot.params['to'] || '/'
  }


  login() {


    this.loginService.login(this.loginForm.value.email, this.loginForm.value.nome).subscribe(
      user => {
        if (user.data != null) {
          this.loginService.user = user.data;
          this.inicioService.participante = user.data;
          this.notificationService.notify(`Bem vindo, ${user.data.nome}`);
        } else {
          this.notificationService.notify(user.errors[0]);
        }
      },
      response => {
        this.notificationService.notify("Não foi possível realizar login! Desculpa.");
      },
      () => {
        this.router.navigate(['/aviso'])
      });

  }
}

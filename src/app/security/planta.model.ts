export class Planta {
    id: number
    nome: String
    nomeCientifico: String
    cor: String
    aceitaCalcada: String
    aceitaJardim: String
    tamCalcada: String
    aceitaFiacao: String
    crescimento: String
    porte: String
    tronco: String
    frutoComestivel: String
    atraiPassaros: String
}
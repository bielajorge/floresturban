import { Routes } from "@angular/router";
import { InicioComponent } from "./inicio/inicio.component";
import { NotFoundComponent } from './not-found/not-found.component';
import { LoginComponent } from './security/login/login.component';
import { EndComponent } from "./end/end.component";
import { AvisoComponent } from "./aviso/aviso.component";

export const ROUTES: Routes = [
    { path: '', component: LoginComponent },
    { path: 'home', component: InicioComponent },
    { path: 'login', component: LoginComponent },

    { path: 'end', component: EndComponent },
    { path: 'aviso', component: AvisoComponent },

    // 404
    { path: '**', component: NotFoundComponent }

]  
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LocationStrategy, HashLocationStrategy, registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';

registerLocaleData(localePt, 'pt-BR')

import { AccordionModule } from 'primeng/accordion';     //accordion and accordion tab
import { DialogModule } from 'primeng/dialog';
import { FormsModule } from '@angular/forms';
import { SharedModule } from './shared/shared.module';

import { Interceptor } from './interceptor.modulo';

import { ROUTES } from './app.routes';

import { AppComponent } from './app.component';

import { LoginComponent } from './security/login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';

import { HeaderComponent } from './header/header.component';
import { UserDetailComponent } from './header/user-detail/user-detail.component';

import { InicioComponent } from './inicio/inicio.component';
import { AlternativasComponent } from './inicio/alternativas/alternativas.component';
import { InicioService } from './inicio/inicio.service';
import { EndComponent } from './end/end.component';
import { AvisoComponent } from './aviso/aviso.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    InicioComponent,
    LoginComponent,
    NotFoundComponent,
    UserDetailComponent,
    AlternativasComponent,
    EndComponent,
    AvisoComponent
  ],
  imports: [
    AccordionModule,
    BrowserModule,
    BrowserAnimationsModule,
    DialogModule,
    FormsModule,
    HttpClientModule,
    Interceptor,
    RouterModule.forRoot(ROUTES),
    SharedModule.forRoot(),
  ],
  providers: [
    InicioService,
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: LOCALE_ID, useValue: 'pt-BR' }],
  bootstrap: [AppComponent]
})
export class AppModule { }

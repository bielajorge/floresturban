import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
    OwlTooltipModule, OwlInputMaskModule, OWL_DIALOG_CONFIG,
    OwlDialogModule, OwlFormFieldModule, OwlInputModule, OwlRippleModule,
} from 'owl-ng';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

import { BottonComponent } from './bottom/bottom.component';
import { InputComponent } from "./input/input.component";
import { RadioComponent } from "./radio/radio.component";
import { RatingComponent } from "./rating/rating.component";

import { DialogComponent } from './dialog/dialog.component';
import { DialogPrimeComponente } from './dialog/prime/dialog-prime.component';
import { BasicComponent } from './dialog/basic/basic.component';
import { DummyDialogComponent } from './dialog/dummy-dialog/dummy-dialog.component';

import { SnackbarComponent } from "./messages/snackbar/snackbar.component";
import { LoggedInGuard } from '../security/loggedin.guard';

import { LoginService } from '../security/login/login.service';
import { NotificationService } from "./messages/notification.service";

import { OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { YearMonthComponent } from "./input/year-month/year-month.component";
import { DialogModule } from 'primeng/dialog';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ToastModule } from 'primeng/toast';


@NgModule({
    declarations: [
        BottonComponent,
        DialogComponent,
        DialogPrimeComponente,
        InputComponent,
        RadioComponent,
        RatingComponent,
        SnackbarComponent,
        BasicComponent,
        DummyDialogComponent,
        YearMonthComponent,

    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        OwlDialogModule,
        OwlRippleModule,
        OwlFormFieldModule,
        OwlInputModule,
        OwlTooltipModule,
        OwlInputMaskModule,
        DialogModule,
        InputTextareaModule,
        ToastModule,
    ],
    exports: [
        BottonComponent,
        DialogComponent,
        DialogPrimeComponente,
        InputComponent,
        RadioComponent,
        RatingComponent,
        SnackbarComponent,
        BasicComponent,
        DummyDialogComponent,
        CommonModule,
        FormsModule,
        YearMonthComponent,
        ReactiveFormsModule,

    ],
    providers: [
        {
            // Global Dialog Config
            // the config would apply to all the dialogs under this module
            provide: OWL_DIALOG_CONFIG, useValue: {
                dialogClass: 'dummy-dialog',
                width: '400px',
            }
        }, {
            // use french locale
            provide: OWL_DATE_TIME_LOCALE, useValue: 'fr'

        }
    ],
    bootstrap: [BasicComponent, DialogPrimeComponente],
    entryComponents: [DummyDialogComponent, DialogPrimeComponente]
})


export class SharedModule {

    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                NotificationService,
                LoginService,
                LoggedInGuard,
            ]

        }
    }
}
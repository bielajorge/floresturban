import { Input, Component, Output, EventEmitter } from "@angular/core";



@Component({
    selector: 'dialog-prime',
    templateUrl: './dialog-prime.component.html',

})

export class DialogPrimeComponente {

    public texto;
    public display;
    public header: string;
    public rows = "5"
    public cols = "35"
    
    @Output() retornoTexo = new EventEmitter();

    showDialog(header: string, texto: string) {
        if (header != null)
            this.header = header;
        else
            this.header = "Mensagem"
        this.display = true;
        this.texto = texto
    }

    closeDialog() {
        this.display = false;
    }

    saveDialog() {
        this.retornoTexo.emit(this.texto);
        this.closeDialog();
    }


}
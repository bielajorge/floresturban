import { Component } from '@angular/core';

/**
 * @title Bottom Sheet Overview
 */
@Component({
    selector: 'mt-bottom',
    templateUrl: 'bottom.component.html'
})
export class BottonComponent {
    transferData: Object = { id: 1, msg: 'Hello' };
    receivedData: Array<any> = [];

    constructor() { }


    transferDataSuccess($event: any) {
        this.receivedData.push($event);
        console.log($event)
    }
}

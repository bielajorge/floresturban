import { Component, OnInit } from '@angular/core';
import { trigger, style, state, transition, animate } from '@angular/animations';

import { NotificationService } from '../notification.service';
import { timer } from 'rxjs';

import { tap, switchMap } from 'rxjs/operators'
import { duration } from 'moment';


@Component({
  selector: 'mt-snackbar',
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.css'],
  animations: [
    trigger('snack-visibility', [
      state('hidden', style({
        opacity: '0',
        visibility: 'hidden',
        position: 'fixed',
        bottom: '50px',
        right: '0px',
        left: '0px'
      })),
      state('visible', style({
        opacity: 1,
        visibility: 'visible',
        position: 'fixed',
        bottom: '50px',
        right: '0px',
        left: '0px'
      })),
      state('growverde', style({
        opacity: 1,
        visibility: 'visible',
        background: '#008000',
        position: 'fixed',
        height: '50px',
        right: '0px',
        left: '80%',
        top: '60px'
      })),
      state('growvermelho', style({
        opacity: 1,
        visibility: 'visible',
        background: '#d6050d',
        position: 'fixed',
        height: '50px',
        right: '10px',
        left: '80%',
        top: '60px',
      })),
      state('growazul', style({
        opacity: '1',
        visibility: 'visible ',
        background: '#0080c0',
        position: 'fixed',
        height: '50px',
        right: '10px',
        left: '80%',
        top: '60px',

      })),
      state('growamarelo', style({
        opacity: 1,
        visibility: 'visible',
        background: '#cccc00',
        position: 'fixed',
        height: '50px',
        right: '0px',
        left: '80%',
        top: '50px',
      })),
      transition('hidden => visible', animate('500ms 0s ease-in')),
      transition('visible => hidden', animate('500ms 0s ease-out')),

      transition('hidden => growverde', animate('500ms 0s ease-in')),
      transition('growverde => hidden', animate('500ms 0s ease-out')),

      transition('hidden => growvermelho', animate('500ms 0s ease-in')),
      transition('growvermelho => hidden', animate('500ms 0s ease-out')),

      transition('hidden => growazul', animate('500ms 0s ease-in')),
      transition('growazul => hidden', animate('500ms 0s ease-out')),

      transition('hidden => growamarelo', animate('500ms 0s ease-in')),
      transition('growamarelo => hidden', animate('500ms 0s ease-out')),
    ])
  ]
})

export class SnackbarComponent implements OnInit {

  message: string
  tipo: number

  snackVisibility: string = 'hidden';

  constructor(private notificationService: NotificationService) { }

  ngOnInit() {
    this.notificationService.tipo
      .pipe(tap(tipo => {
        this.tipo = tipo
      }),
        switchMap(message => timer(3000))
      ).subscribe(timer => this.snackVisibility = 'hidden')


    this.notificationService.notifier
      .pipe(tap(message => {
        this.message = message
        this.change(this.tipo)
      }),
        switchMap(message => timer(3000))
      ).subscribe(timer => this.snackVisibility = 'hidden')
  }

  change(tipo: number) {
    switch (tipo) {
      case 0:
        this.snackVisibility = 'visible'
        break;
      case 1:
        this.snackVisibility = 'growverde'
        break;
      case 2:
        this.snackVisibility = 'growvermelho'
        break;
      case 3:
        this.snackVisibility = 'growazul'
        break;
      case 4:
        this.snackVisibility = 'growamarelo'
        break;
      default:
        this.snackVisibility = 'visible'
        break;
    }
  }
}

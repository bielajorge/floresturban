import { EventEmitter } from '@angular/core';

export class NotificationService {
    notifier = new EventEmitter<string>()
    tipo = new EventEmitter<number>()

    notify(message: string, tipo?: number) {
        this.tipo.emit(tipo)
        this.notifier.emit(message)
    }

}
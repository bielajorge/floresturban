import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { FLORESTA_API } from 'app/app.api';
import { Participante } from 'app/security/login/participantes.model';

@Injectable()
export class InicioService {

    plantas: any
    participante: Participante

    constructor(
        private http: HttpClient
    ) { }

    getPergunta(question: number, response: number) {
        return this.http.post<any>(`${FLORESTA_API}/quiz/${question}/${response}`, this.participante);
    }

    setResposta(pergunta: number, resposta: number) {
        console.log("Resposta ", resposta, " Para pergunta ", pergunta)
        return this.http.post<any>(`${FLORESTA_API}/quiz/${pergunta}/${resposta}`, this.participante);
    }
}

import { Component, OnInit, Input } from '@angular/core';
import { Participante } from 'app/security/login/participantes.model';
import { InicioService } from './inicio.service';
import { NotificationService } from 'app/shared/messages/notification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'mt-home',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  participante: Participante
  plantas = new Array();
  perguntas = new Array();

  titulo = "Você tem algum problema respiratório?"
  idPergunta = 0
  alternativa = 0
  resposta = 0
  nova = ""
  valor = 0
  perguntaAnterior = 0;

  constructor(
    private inicioService: InicioService,
    private notificationService: NotificationService,
    private router: Router) { }

  ngOnInit() {
    this.getPergunta();
  }

  anterior() {
    if (this.idPergunta != this.perguntaAnterior && this.idPergunta > this.perguntaAnterior) {
      this.idPergunta = this.perguntaAnterior;
    } else {
      if (this.idPergunta != 0)
        this.idPergunta--;
    }
    this.nova = "";

    this.titulo = this.perguntas[this.idPergunta];
    this.selecionaAlternativas();
  }

  proxima() {
    this.perguntaAnterior = this.idPergunta;
    if (this.resposta > -1 || this.idPergunta == 6) {
      this.inicioService.setResposta(this.idPergunta, this.resposta).subscribe(
        data => {

          this.idPergunta++;
          this.valor = -1;
          this.verificaPergunta();
          this.selecionaAlternativas();
          this.resposta = -1;
          this.nova = "";

          this.titulo = this.perguntas[this.idPergunta];
          this.inicioService.plantas = data.data.plantas

          if (data.data != null) {
            this.inicioService.participante = data.data;
            this.plantas = data.data.plantas
            console.log(data)
          } else {
            this.notificationService.notify(data.errors[0]);
          }

          if (this.idPergunta == 13) {
            this.router.navigate(['/end'])
          }
        },
        response => {
          this.notificationService.notify("Sua resposta não foi computada! Desculpa.")
        }
      );
    } else
      this.notificationService.notify("Selecione uma alternativa primeiro")
  }

  selecionaAlternativas() {
    this.valor = -1;
    if (this.idPergunta < 7 || this.idPergunta > 10) {
      this.valor = -1;
      if (this.idPergunta == 6)
        this.alternativa = 5
      else
        this.alternativa = 0

    } else if (this.idPergunta == 7) {
      this.alternativa = 4
    } else if (this.idPergunta == 8) {
      this.alternativa = 3
    } else if (this.idPergunta == 9) {
      this.alternativa = 2
    } else if (this.idPergunta == 10) {
      this.alternativa = 1
    }
  }

  reciverFeedback(valor: string) {

    this.nova = valor


    if (valor === "Sim"
      || valor === "Acima de 5m²"
      || valor === "2m"
      || valor === "Calçada"
      || valor === "Casa") {

      this.resposta = 0;

    } else if (valor === "Não"
      || valor === "Acima de 20m²"
      || valor === "2,5m"
      || valor === "Quintal/Jardim"
      || valor === "Minha empresa") {

      this.resposta = 1;

    } else if (valor === "Mais de 40m²"
      || valor === "3m"
      || valor === "Meu trabalho") {

      this.resposta = 2;

    } else if (valor === "4m"
      || valor === "Instituição que frequento") {

      this.resposta = 3;

    } else if (valor === "5m ou mais") {
      this.resposta = 4;
    }
  }

  verificaPergunta() {
    if (this.idPergunta == 4) {//"Que maravilha! Que tal plantar outra?"
      if (this.resposta == 1)
        this.idPergunta++;
    } else if (this.idPergunta == 5) {//"Gostaria de plantar sua primeira árvore?"

      if (this.resposta == 0)
        this.idPergunta += 2
      else
        this.idPergunta++
    } else if (this.idPergunta == 6) {// "Que lugar prefere? (Para plantar sua nova árvore)",
      if (this.resposta == 0) {
        this.idPergunta++;
      }
    } else if (this.idPergunta == 9) {
      if (this.resposta == 1)
        this.idPergunta++;
    } else if (this.idPergunta == 10) {
      this.idPergunta++
    } else if (this.idPergunta == 11) {
      this.idPergunta++
    }
  }

  getPergunta() {
    this.perguntas = [
      "Você tem algum problema respiratório?",//0
      "Você gosta de sombra?",//1
      "Você se incomoda com a poeira?",//2
      "Você já plantou/cuidou de uma árvore?",//3
      "Que maravilha! Que tal plantar outra?",//4
      "Gostaria de plantar sua primeira árvore?",//5
      "Que pena! Podemos prosseguir? Quem sabe você tenha uma boa surpresa.",//6
      "Que lugar prefere? (Para plantar sua nova árvore)",//7
      "Em qual espaço?",//8
      "Qual o tamanho aproximado da calçada?",//9
      "Que espaço tem disponível neste quintal/jardim?",//10
      "Tem fiação aérea na calçada?",//11
      "Você gosta de árvores frutíferas?"//12
    ]
  }
}

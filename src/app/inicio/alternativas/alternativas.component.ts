import { Component, OnInit, Input, EventEmitter, Output, ɵConsole } from '@angular/core';
import { RadioOption } from 'app/shared/radio/radio.model';


@Component({
  selector: 'mt-alternativas',
  templateUrl: './alternativas.component.html',
  styleUrls: ['./alternativas.component.css'],
})
export class AlternativasComponent implements OnInit {

  @Input() recebeId;
  @Input() value;
  @Output() resposta = new EventEmitter();

  onChange: any;
  alternativas = new Array();
  alternativa = new Array();
  idPergunta = 0;

  constructor() { }

  ngOnInit() {

    this.alternativas = [
      ["Sim", "Não"],
      ["Acima de 5m²", "Acima de 20m²", "Mais de 40m²"],
      ["2m", "2,5m", "3m", "4m", "5m ou mais"],
      ["Calçada", "Quintal/Jardim"],
      ["Casa", "Minha empresa", "Meu trabalho", "Instituição que frequento"]
    ]

    this.alternativa = this.alternativas[0]
  }

  setValue(value: any) {
    this.value = value
    this.feedback();
  }

  registerOnTouched(fn: any): void { }

  setDisabledState?(isDisabled: boolean): void { }

  feedback() {

    this.resposta.emit(this.value);

  }

}



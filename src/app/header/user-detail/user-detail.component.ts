import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../security/login/login.service'
import { Participante } from 'app/security/login/participantes.model';

@Component({
  selector: 'mt-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  hidden: boolean;
  visible: boolean

  constructor(private loginService: LoginService) { }

  ngOnInit() { }

  user(): Participante {
    return this.loginService.user
  }

  isLoggedIn(): boolean {
    return this.loginService.isLoggedIn()
  }

  login() {
    this.loginService.handleLogin()
  }

  logout() {
    this.loginService.logout()
  }

  toggleHidden(): void {
    this.hidden = !this.hidden;
  }
}

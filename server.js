
const express = require('express');
const path = require('path');
const http = require('http');
const app = express();

app.use(express.static(__dirname + '/dist/'));

app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname));
});

const server = http.createServer(app);

server.listen(process.env.PORT || 4200, () => console.log('runing'));
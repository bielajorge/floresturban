import * as jsonServer from 'json-server';
import { Express } from 'express'

import * as fs from 'fs'
import * as https from 'https'
import { handleAuthentication } from './auth';

const server: Express = jsonServer.create()
const router = jsonServer.router('db.json')
const middlewares = jsonServer.defaults()

// Set default middlewares (logger, static, cors and no-cache)
server.use(middlewares)
//you can use json server
server.use(jsonServer.bodyParser)

//middlewares para login
server.post('/login', handleAuthentication)

//Use default route
server.use(router)


const options = {
  cert: fs.readFileSync('./backend/keys/cert.pem'),
  key: fs.readFileSync('./backend/keys/key.pem')
}

https.createServer(options, server).listen(3001, () => {
  console.log('JSON Server is runing on https/localhost:3001')
})
